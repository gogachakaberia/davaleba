fun main() {
    val x = fraction(5.0, 6.0)
    val y = fraction(7.0, 8.0)
    println(x)
    println(y)
    println(x.add(y))
    println(x.minus(y))
    println(x.multifly(y))
    println(x.divide(y))
}

class fraction(val numenator: Double, val denominator: Double) {
    override fun toString(): String {
        return "$numenator $denominator"
    }

    override fun equals(other: Any?): Boolean {
        if (other is fraction) {
            return (numenator * other.denominator == other.numenator * denominator)
        }
        return false
    }

    fun add(other: fraction): fraction {
        val newDenominator = denominator * other.denominator
        val newNumerator1 = newDenominator / denominator * numenator
        val newNumerator2 = newDenominator / other.denominator * other.numenator
        return fraction(newNumerator1 + newNumerator2, newDenominator)

    }

    fun multifly(other: fraction): fraction {
        val newNumerator = numenator * other.numenator
        val newDenominator = denominator * other.denominator
        return fraction(newNumerator, newDenominator)
    }

    fun minus(other: fraction): fraction {
        val newDenominator = denominator * other.denominator
        val newNumerator1 = newDenominator / denominator * numenator
        val newNumerator2 = newDenominator / other.denominator * other.numenator
        return fraction(newNumerator1 - newNumerator2, newDenominator)
    }

    fun divide(other: fraction): fraction {
        val newNumerator = numenator * other.denominator
        val newDenominator = denominator * other.numenator
        return fraction(newNumerator, newDenominator)

    }
}